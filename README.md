**The header-only config library**

- [ ] JSON support
- [ ] YAML support
- [ ] INI support
- [ ] Java properties support
- [ ] XML support

<!-- 2022.12.11 Just in case GitLab doesn't delete the project due to inactivity. -->
