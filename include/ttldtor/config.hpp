#ifndef TTLDTOR_CONFIG_HPP
#define TTLDTOR_CONFIG_HPP

#include <string>
#include <vector>
#include <any>

namespace ttldtor {

  struct StorageFormat {};

  struct JsonStorageFormat : StorageFormat {};

  struct IniStorageFormat : StorageFormat {};

  struct YamlStorageFormat : StorageFormat {};

  struct JavaPropertiesStorageFormat : StorageFormat {};

  struct XmlStorageFormat : StorageFormat {};

  struct ConfigRecord {};

  struct FieldDescription {};

  template <typename ValueType>
  struct TypedFieldDescription : FieldDescription {
  };

  template <typename ConfigStorageFormat>
  struct BasicConfig {
    std::vector<std::any> fieldDescriptions_;

    template <typename FieldDescriptionType>
    friend BasicConfig& operator << (BasicConfig& config, const FieldDescriptionType& fieldDescription) {
      if constexpr (std::is_convertible<FieldDescriptionType, FieldDescription>::value) {
        config.fieldDescriptions_.emplace_back(fieldDescription);
      }

      return config;
    }

    template <typename Index>
    ConfigRecord operator [](Index index) {
      return {};
    }

    template <typename InputStream>
    friend InputStream& operator >> (InputStream& is, BasicConfig& config) {
      return is;
    }
  };

  struct Config {
  };
}

#endif // TTLDTOR_CONFIG_HPP