#include <iostream>
#include <ttldtor/config.hpp>

int main() {
  using namespace ttldtor;

  BasicConfig<JsonStorageFormat> config;

  std::cout << "123\n";
  std::cin >> config;

  config
    << TypedFieldDescription<int>{}
    << TypedFieldDescription<double>{}
    << 3
    << "123"
    << TypedFieldDescription<BasicConfig<JsonStorageFormat>>{};

  std::cout << config.fieldDescriptions_.size();

  return 0;
}